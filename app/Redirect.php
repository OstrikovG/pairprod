<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Redirect extends Model
{
    protected $table = 'redirects';

    protected $fillable = ['headers', 'user_agent', 'ip', 'country', 'link_id'];

    public function link()
    {
        return $this->belongsTo('App\Link');
    }
}
