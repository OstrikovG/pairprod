<?php

namespace App\Repositories;

use App\Link;
use App\Redirect;

class RedirectRepository extends BaseRepository
{
    protected $model;

    public function __construct(Redirect $redirect)
    {
        $this->model = $redirect;
    }
}
