<?php

namespace App\Repositories;

use App\Link;

class LinkRepository extends BaseRepository
{
    protected $model;

    public function __construct(Link $link)
    {
        $this->model = $link;
    }
}
