<?php

namespace App\Services;

use App\Repositories\LinkRepository;

class LinkService extends BaseService
{
    private $linkRepository;

    public function __construct(LinkRepository $linkRepository)
    {
        $this->repo = $linkRepository;
        $this->linkRepository = $linkRepository;
    }

    public function generateShortLink($minLenght = 6, $maxLength = 10) {
        $length = rand($minLenght, $maxLength);
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
