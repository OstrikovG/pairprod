<?php

namespace App\Services;

use App\Link;
use App\Repositories\RedirectRepository;
use Illuminate\Support\Facades\Redirect;

class RedirectService extends BaseService
{
    private $redirectRepository;

    public function __construct(RedirectRepository $redirectRepository)
    {
        $this->repo = $redirectRepository;
        $this->redirectRepository = $redirectRepository;
    }

    public function getResponseByLink(?Link $link)
    {
        if (isset($link)) {
            $response =  Redirect::to($link->link);
        } else{
            $response = response()->json(null, 404);
        }
        return $response;
    }
}
