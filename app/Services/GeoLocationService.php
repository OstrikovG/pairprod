<?php

namespace App\Services;

class GeoLocationService
{
    public function getUserCountryByIp($ip): string {
        $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}"));
        if (method_exists($details, 'country')) {
            $country = $details->country;
        } else{
            $country = 'not found';
        }
        return $country;
    }
}
