<?php

namespace App\Services;

use App\Link;
use App\Repositories\LinkRepository;
use App\Repositories\RedirectRepository;

class StatisticService extends BaseService
{
    private $linkRepository;
    private $redirectRepository;

    public function __construct(LinkRepository $linkRepository, RedirectRepository $redirectRepository)
    {
        $this->linkRepository = $linkRepository;
        $this->redirectRepository = $redirectRepository;
    }

    public function findAllLinksWithRedirectsCount()
    {
        return Link::withCount('redirects')
            ->get();
    }
}
