<?php

namespace App\Http\Controllers;

use App\Link;
use App\Services\GeoLocationService;
use App\Services\RedirectService;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

class RedirectController extends Controller
{


    /**
     * @var RedirectService
     */
    protected $redirectService;
    /**
     * @var GeoLocationService
     */
    private $geoLocationService;

    /**
     * RedirectController constructor.
     * @param RedirectService $redirectService
     */
    public function __construct(
        RedirectService $redirectService,
        GeoLocationService $geoLocationService)
    {
        $this->redirectService = $redirectService;
        $this->geoLocationService = $geoLocationService;
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function redirect(Request $request)
    {
        $link = Link::where('short_link', $request->shortLink)->first();
        $link_id = isset($link) ? $link->id : null;
        $ip = $request->ip();

        $response = $this->redirectService->getResponseByLink($link);

        $input = [
            'headers' => $request->headers,
            'user_agent' => $request->userAgent(),
            'ip' => $ip,
            'country' => $this->geoLocationService->getUserCountryByIp($ip),
            'link_id' => $link_id
        ];

        $this->redirectService->create($input);

        return $response;
    }

}
