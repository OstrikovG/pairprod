<?php

namespace App\Http\Controllers;

use App\Services\GeoLocationService;
use App\Services\StatisticService;
use Illuminate\Http\Request;

class StatisticController extends Controller
{
    /** @var StatisticService */
    private $statisticService;

    public function __construct(StatisticService $staticticService)
    {
        $this->statisticService = $staticticService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $links = $this->statisticService->findAllLinksWithRedirectsCount();
        return response()->json($links, 200);
    }
}
