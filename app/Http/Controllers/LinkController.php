<?php

namespace App\Http\Controllers;

use App\Link;
use App\Services\LinkService;
use Illuminate\Http\Request;

class LinkController extends Controller
{

    /**
     * @var LinkService
     */
    protected $linkService;

    /**
     * LinkController constructor.
     * @param LinkService $linkService
     */
    public function __construct(LinkService $linkService)
    {
        $this->linkService = $linkService;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $link = Link::where('link', $request->link)->first();
        if (empty($link)) {
            $link = $this->linkService->create([
                'link' => $request->link,
                'short_link' => $this->linkService->generateShortLink()
            ]);
        }
        return response()->json($link, 201);
    }
}
