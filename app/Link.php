<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $table = 'links';

    protected $fillable = ['link', 'short_link'];

    public function redirects()
    {
        return $this->hasMany('App\Redirect');
    }
}
