# pairprod

**Start the project**

1. Add `pairprod.loc` to your hosts

2. Enter console command to run the project:

        docker-compose up

3. Run migrations:

    docker-compose exec php doctrine:migrations:migrate

**Use api queries**

Use api queries documentation:
    
   _****https://documenter.getpostman.com/view/4304970/SVtbQ5oa?version=latest****_
